﻿using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Views.Vendas
{
    public partial class Vendas : Form
    {
        private readonly SalesService _salesService;
        private ICollection<Sale> Sales = new List<Sale>();
        public Vendas()
        {
            InitializeComponent();
            _salesService = new SalesService();
        }

        private void Vendas_Load(object sender, EventArgs e)
        {
            Pesquisa();
        }
        private void Pesquisa()
        {
            // Fazendo a pesquisa das vendas ou retornando todos se o input for vazio
            SalesList.Items.Clear();
            Sales = _salesService.GetAll(Pesquisar.Text.Trim().Split("\n".ToCharArray()).Last());
            foreach (var sale in Sales)
            {
                var item = new ListViewItem { Text = sale.Client.Fullname };
                item.SubItems.Add(sale.User.Username);
                item.SubItems.Add("R$" + sale.Price.ToString("N2"));
                item.SubItems.Add(sale.BuyProducts.Count > 0 ?
                    sale.BuyProducts.Select(x => x.Product.Name).Aggregate((a, b) => $"{a}, {b}")
                    : "");
                item.SubItems.Add(sale.BuyPacks.Count > 0 ?
                    sale.BuyPacks.Select(x => x.Pack.Name).Aggregate((a, b) => $"{a}, {b}")
                    : "");
                SalesList.Items.Add(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void Vendas_DoubleClick(object sender, EventArgs e)
        {
        }

        private void SalesList_DoubleClick(object sender, EventArgs e)
        {
            // Quando der o click duplo vai abrir a tela de Gerenciamento de venda
            if (SalesList.SelectedItems.Count == 0) return;
            var id = Sales.ElementAt(SalesList.SelectedItems[0].Index).ID;
            var form = new VendaGerenciamento(id);
            form.ShowDialog();
            Pesquisa();
        }
    }
}
