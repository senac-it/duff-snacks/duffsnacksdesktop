﻿using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Views.Vendas
{
    public partial class VendaGerenciamento : Form
    {
        // Instanciando os serviços necessarios
        private readonly ProductsService _productsService = new ProductsService();
        private readonly PacksService _packsServices = new PacksService();
        private readonly ClientsService _clientsServices = new ClientsService();
        private readonly SalesService _salesService = new SalesService();

        private ICollection<Pack> Packs = new List<Pack>();
        private ICollection<Product> Products = new List<Product>();
        private ICollection<Client> Clients = new List<Client>();

        private int ID = -1;
        private double Price = 0;
        public VendaGerenciamento()
        {
            InitializeComponent();
        }
        public VendaGerenciamento(int id)
        {
            InitializeComponent();
            ID = id;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void VendaGerenciamento_Load(object sender, EventArgs e)
        {
            // Limpando os campos de seleção
            SelectPacotes.Items.Clear();
            SelectProduct.Items.Clear();
            SelectClient.Items.Clear();

            // Pegando todos os pacotes da api e colocando no seletor de pacotes
            Packs = _packsServices.GetAll();
            foreach (var pack in Packs)
            {
                var price = pack.Price.ToString("N2");
                SelectPacotes.Items.Add($"{pack.ID} - {pack.Name} R${price}");
            }

            // Pegando todos os Produtos da api e colocando no seletor de Produtos
            Products = _productsService.GetAll();
            foreach (var product in Products)
            {
                var price = product.Price.ToString("N2");
                SelectProduct.Items.Add($"{product.ID} - {product.Name} R${price}");
            }

            // Pegando todos os Clientes da api e colocando no seletor de Clientes
            Clients = _clientsServices.GetAll();
            foreach (var client in Clients)
                SelectClient.Items.Add($"{client.Fullname} - {client.Email}");

            // Checando se alguma venda foi selecionado, se nao foi ele retorna e nao faz nada
            if (ID == -1) return;
            // Pegando a venda para a atualização
            var sale = _salesService.GetOne(ID);
            // Checando se existe a venda, se nao ele retorna e nao faz nada
            if (sale == null) return;
            // Habilitando os botões necessarios e passando os valores da venda selecionada
            ButtonDeletar.Enabled = true;
            InputPrice.Text = "R$" + sale.Price.ToString("N2");
            Price = sale.Price;
            InputID.Text = sale.ID.ToString();
            // Aplicando o cliente da venda selecionada ao campo de seleção de cliente
            for (int i = 0; i < Clients.Count; i++)
            {
                var client = Clients.ElementAt(i);
                if (client.ID == sale.ClientID) SelectClient.SelectedIndex = i;
            }
            // Limpando a lista de produtos selecionados e passando os produtos da venda selecionada
            ListaProdutos.Items.Clear();
            foreach (var product in sale.BuyProducts)
            {
                var price = product.Product.Price.ToString("N2");
                ListaProdutos.Items.Add($"{product.ProductID} - {product.Product.Name} R${price}");
            }
            // Limpando a lista de Pacotes selecionados e passando os Pacotes da venda selecionada
            ListaPacotes.Items.Clear();
            foreach (var pack in sale.BuyPacks)
            {
                var price = pack.Pack.Price.ToString("N2");
                ListaPacotes.Items.Add($"{pack.PackID} - {pack.Pack.Name} R${price}");
            }
            // Desativando o campo de seleção do cliente, porque ele nao pode ser alterado caso seja uma atualização
            SelectClient.Enabled = false;
        }

        private void ButtonAddProduct_Click(object sender, EventArgs e)
        {
            if (SelectProduct.SelectedIndex != -1)
            {
                // Adicionando o produto selecionado a lista
                var splited = SelectProduct.SelectedItem.ToString().Split("R$".ToCharArray());
                Price += double.Parse(splited.Last());
                ListaProdutos.Items.Add(SelectProduct.SelectedItem);
                InputPrice.Text = "R$" + Price.ToString("N2");
            }
        }

        private void ButtonRemoverProduct_Click(object sender, EventArgs e)
        {
            // Removendo o produto selecionado da lista
            var splited = ListaProdutos.SelectedItem.ToString().Split("R$".ToCharArray());
            Price -= double.Parse(splited.Last());
            ListaProdutos.Items.Remove(ListaProdutos.SelectedItem);
            ButtonRemoverProduct.Enabled = false;
            InputPrice.Text = "R$" + Price.ToString("N2");
        }

        private void RemovePacote_Click(object sender, EventArgs e)
        {
            // Removendo um pacote selecionado da lista
            var splited = ListaPacotes.SelectedItem.ToString().Split("R$".ToCharArray());
            Price -= double.Parse(splited.Last());
            ListaPacotes.Items.Remove(ListaPacotes.SelectedItem);
            RemovePacote.Enabled = false;
            InputPrice.Text = "R$" + Price.ToString("N2");
        }

        private void AddPacote_Click(object sender, EventArgs e)
        {
            if (SelectPacotes.SelectedIndex != -1)
            {
                // Adicionando um pacote selecionado a lista
                var splited = SelectPacotes.SelectedItem.ToString().Split("R$".ToCharArray());
                Price += double.Parse(splited.Last());
                ListaPacotes.Items.Add(SelectPacotes.SelectedItem);
                InputPrice.Text = "R$" + Price.ToString("N2");
            }
        }

        private void ListaProdutos_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButtonRemoverProduct.Enabled = true;
        }

        private void ListaPacotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            RemovePacote.Enabled = true;
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonDeletar_Click(object sender, EventArgs e)
        {
            // Deletando a venda
            if (_salesService.DeleteOne(ID)) Close();
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            var price = Price;
            var clientID = Clients.ElementAt(SelectClient.SelectedIndex).ID;

            var products = new List<int>();

            foreach (var product in ListaProdutos.Items)
                products.Add(int.Parse(product.ToString().Split("-".ToCharArray())[0].Trim()));

            var packs = new List<int>();

            foreach (var pack in ListaPacotes.Items)
                packs.Add(int.Parse(pack.ToString().Split("-".ToCharArray())[0].Trim()));

            if (ID == -1)
            {
                if (_salesService.CreateOne(
                    price,
                    clientID,
                    packs.Select(x => x).ToList(),
                    products.Select(x => x).ToList()
                    )) Close();
            }
            else
            {
                if (_salesService.UpdateOne(
                    ID,
                    price,
                    packs.Select(x => x).ToList(),
                    products.Select(x => x).ToList()
                )) Close();
            }
        }
    }
}
