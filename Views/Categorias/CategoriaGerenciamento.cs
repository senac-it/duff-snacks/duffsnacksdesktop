﻿using DuffSnacksDesktop.Configs;
using DuffSnacksDesktop.Models;
using RestSharp;
using System;
using System.Windows.Forms;
using Newtonsoft.Json;
using DuffSnacksDesktop.Models.DTOs;
using System.Linq;
using DuffSnacksDesktop.Services;

namespace DuffSnacksDesktop.Views.Categorias
{
    public partial class CategoriaGerenciamento : Form
    {
        private readonly CategoriesService _categoriesService = new CategoriesService();
        private readonly int ID = -1;
        public CategoriaGerenciamento()
        {
            InitializeComponent();
        }
        public CategoriaGerenciamento(int id)
        {
            InitializeComponent();
            ID = id;
        }

        private void CategoriaCadastro_Load(object sender, EventArgs e)
        {
            // Checando se tem ID para atualizar a categoria
            if (ID == -1) return;
            // Pegando uma categoria para atualizar
            var category = _categoriesService.GetOne(ID);
            // Checando se existe categoria se não retorna e não faz nada
            if (category == null) return;
            // Habilitando os botões necessarios e passando os valores da categoria selecionada para os inputs
            ButtonDelete.Enabled = true;
            InputID.Text = category.ID.ToString();
            InputName.Text = category.Name;
            InputDescription.Text = category.Description;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (ID == -1)
            {
                // Criando uma categoria se o ID for = -1
                if (_categoriesService.CreateOne(InputName.Text, InputDescription.Text)) Close();
            }
            else
            {
                // Atualizando a categoria se o ID nao for = -1
                if (_categoriesService.UpdateOne(ID, InputName.Text, InputDescription.Text)) Close();
            }
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            // So vai apagar a categoria e fechar a janela
            if (_categoriesService.DeleteOne(ID)) Close();
        }
    }
}
