﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;
using RestSharp;

namespace DuffSnacksDesktop.Views.Categorias
{
    public partial class Categorias : Form
    {
        private readonly CategoriesService _categoriesService = new CategoriesService();
        private ICollection<Category> listCategories = new List<Category>();

        public Categorias()
        {
            InitializeComponent();
        }

        private  void Categorias_Load(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            
        }

        private void CategoryList_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void Pesquisa()
        {
            // Executando pesquisa, se nao tiver nada vai retornar todas as categorias
            CategoryList.Items.Clear();
            listCategories = _categoriesService.GetAll(Pesquisar.Text.Trim().Split("\n".ToCharArray()).Last());
            foreach (var category in listCategories)
            {
                var item = new ListViewItem { Text = category.Name };
                item.SubItems.Add(category.Description);
                CategoryList.Items.Add(item);
            }
        }

        private void CategoryList_DoubleClick(object sender, EventArgs e)
        {
            // Quando um item for selecionado com 2 clicks vai abrir a janela de gerenciamento
            if (CategoryList.SelectedItems.Count == 0) return;
            var id = listCategories.ElementAt(CategoryList.SelectedItems[0].Index).ID;
            var form = new CategoriaGerenciamento(id);
            form.ShowDialog();
            Pesquisa();
        }
    }
}
