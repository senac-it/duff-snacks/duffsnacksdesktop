﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DuffSnacksDesktop.Configs;
using DuffSnacksDesktop.Views.Categorias;
using DuffSnacksDesktop.Views.Pacotes;
using DuffSnacksDesktop.Views.Produtos;
using DuffSnacksDesktop.Views.TelaInicial;
using DuffSnacksDesktop.Views.Usuarios;
using DuffSnacksDesktop.Views.Vendas;

namespace DuffSnacksDesktop.Views.Principal
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Abrindo a tela de listagem de pacotes
            var form = new Views.Pacotes.Pacotes();
            form.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Abrindo a tela de listagem de produtos
            var form = new Views.Produtos.Produtos();
            form.ShowDialog();    
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Abrindo a tela de listagem de categorias
            var form  = new Views.Categorias.Categorias();
            form.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Abrindo a tela de listagem de Usuários
            var form = new Views.Usuarios.Usuarios();
            form.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            // Abrindo a tela de Gerenciamento de Pacote
            var form = new PacoteGerenciamento();
            form.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            // Abrindo a tela de Gerenciamento de Produto
            var form = new ProdutoGerenciamento();
            form.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            // Abrindo a tela de Gerenciamento de Categoria
            var form = new CategoriaGerenciamento();
            form.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            // Abrindo a tela de Gerenciamento de Usuario
            var form = new UsuarioGerenciamento();
            form.ShowDialog();
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            // Abrindo a tela de Listagem de vendas
            var Form = new Views.Vendas.Vendas();
            Form.ShowDialog();
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            // Abrindo a tela de Gerenciamento de vendas
            var form = new VendaGerenciamento();
            form.ShowDialog();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            // Splash
            var splash = new Splash();
            splash.ShowDialog();
            // Abrindo a tela de login
            var form = new UsuarioLogin();
            form.ShowDialog();
            // Fechando o app se não estiver autenticado
            if (Api.GetUserID == -1) Application.Exit();
        }

        private void button11_Click(object sender, EventArgs e)
        {

            // Abrindo a tela de listagem de clientes
            var form = new Clientes.Clientes();
            form.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            // Abrindo a tela de gerenciamento de cliente
            var form = new Clientes.ClienteGerenciamento();
            form.ShowDialog();
        }
    }
}
