﻿using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;
using DuffSnacksDesktop.Views.Categorias;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Views.Clientes
{
    public partial class Clientes : Form
    {
        private ICollection<Client> Clients= new List<Client>();
        private readonly ClientsService _clientsService= new ClientsService();
        public Clientes()
        {
            InitializeComponent();
        }

        private void Clientes_Load(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void Pesquisa()
        {
            // Fazendo pesquisa dos clientes ou retornando todos se o input for vazio
            ClientsList.Items.Clear();
            Clients = _clientsService.GetAll(Pesquisar.Text.Trim().Split("\n".ToCharArray()).Last());
            foreach (var client in Clients)
            {
                var item = new ListViewItem { Text = client.Fullname };
                item.SubItems.Add(client.Email);
                item.SubItems.Add(client.Document);
                item.SubItems.Add(client.Address);
                ClientsList.Items.Add(item);
            }
        }

        private void ClientsList_DoubleClick(object sender, EventArgs e)
        {
            // Quando der click duplo ele vai abrir a tela de Gerenciamento de cliente
            if (ClientsList.SelectedItems.Count == 0) return;
            var id = Clients.ElementAt(ClientsList.SelectedItems[0].Index).ID;
            var form = new ClienteGerenciamento(id);
            form.ShowDialog();
            Pesquisa();
        }
    }
}
