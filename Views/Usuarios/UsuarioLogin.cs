﻿using DuffSnacksDesktop.Configs;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Models.DTOs;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Views.Usuarios
{
    public partial class UsuarioLogin : Form
    {
        public UsuarioLogin()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // Tentando fazer o login
                var client = Api.Client("/Auth/Login");
                var request = new RestRequest();
                request.AddBody(new
                {
                    username = InputUsuario.Text.Trim(),
                    password = InputSenha.Text.Trim(),
                });
                var response = client.Post<AuthLoginResponseDTO>(request);
                Api.SaveToken(response.Token, response.ID);
                Close();
            }
            catch
            {
                // Mostrando essa mensagem se o login der errado
                MessageBox.Show("Não foi possivel fazer login");
            }
        }
    }
}
