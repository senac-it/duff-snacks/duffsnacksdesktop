﻿using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop
{
    public partial class UsuarioGerenciamento : Form
    {
        private int ID = -1;
        private ICollection<Role> Roles= new List<Role>();
        private readonly UsersService _usersService = new UsersService();
        public UsuarioGerenciamento()
        {
            InitializeComponent();
        }
        public UsuarioGerenciamento(int id)
        {
            InitializeComponent();
            ID = id;
        }

        private void TelaCadastro_Load(object sender, EventArgs e)
        {
            // Pegando todos os cargos e passando para o seletor de cargos
            SelectCargo.Items.Clear();
            Roles= _usersService.GetAllRoles();
            foreach (var role in Roles)
                SelectCargo.Items.Add(role.Name);

            // Checando se o ID e valido
            if (ID == -1) return;

            //  Pegando o usuario para atualizar
            var user = _usersService.GetOne(ID);
            if (user == null) return;

            // Habilitando os botões necessarios e passando os valores aos inputs
            ButtonDelete.Enabled = true;
            InputUser.Enabled = false;
            InputID.Text = user.ID.ToString();
            InputUser.Text = user.Username;
            for (int i = 0; i < Roles.Count; i++)
            {
                var role = Roles.ElementAt(i);
                if (role.ID == user.RoleID)
                {
                    SelectCargo.SelectedIndex = i;
                    break;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ID == -1)
            {
                // Criando o Usuário se o ID for invalido
                if (_usersService.CreateOne(new User
                {
                    Username = InputUser.Text,
                    Password = InputSenha.Text,
                    RoleID = Roles.ElementAt(SelectCargo.SelectedIndex).ID,
                })) Close();
            }
            else
            {
                // Atualizando o usuario se o ID for valido
                if (_usersService.UpdateOne(ID, new User
                {
                    Username = InputUser.Text,
                    Password = InputSenha.Text,
                    RoleID = Roles.ElementAt(SelectCargo.SelectedIndex).ID,
                })) Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            // Deletando o usuario e fechando a tela
            if (_usersService.DeleteOne(ID)) Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
