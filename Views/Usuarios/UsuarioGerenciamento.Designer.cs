﻿namespace DuffSnacksDesktop
{
    partial class UsuarioGerenciamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.InputUser = new System.Windows.Forms.TextBox();
            this.InputSenha = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ButtonCadastrar = new System.Windows.Forms.Button();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.SelectCargo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonDelete = new System.Windows.Forms.Button();
            this.InputID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usuário";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Senha";
            // 
            // InputUser
            // 
            this.InputUser.Location = new System.Drawing.Point(12, 64);
            this.InputUser.Name = "InputUser";
            this.InputUser.Size = new System.Drawing.Size(316, 20);
            this.InputUser.TabIndex = 3;
            // 
            // InputSenha
            // 
            this.InputSenha.Location = new System.Drawing.Point(12, 103);
            this.InputSenha.Name = "InputSenha";
            this.InputSenha.Size = new System.Drawing.Size(316, 20);
            this.InputSenha.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(116, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 7;
            // 
            // ButtonCadastrar
            // 
            this.ButtonCadastrar.Location = new System.Drawing.Point(12, 190);
            this.ButtonCadastrar.Name = "ButtonCadastrar";
            this.ButtonCadastrar.Size = new System.Drawing.Size(87, 23);
            this.ButtonCadastrar.TabIndex = 8;
            this.ButtonCadastrar.Text = "Salvar";
            this.ButtonCadastrar.UseVisualStyleBackColor = true;
            this.ButtonCadastrar.Click += new System.EventHandler(this.button1_Click);
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.Location = new System.Drawing.Point(241, 190);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(87, 23);
            this.ButtonCancelar.TabIndex = 9;
            this.ButtonCancelar.Text = "Cancelar";
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.button2_Click);
            // 
            // SelectCargo
            // 
            this.SelectCargo.FormattingEnabled = true;
            this.SelectCargo.Items.AddRange(new object[] {
            "Lorem",
            "Lorem",
            "Lorem",
            "Lorem"});
            this.SelectCargo.Location = new System.Drawing.Point(12, 142);
            this.SelectCargo.Name = "SelectCargo";
            this.SelectCargo.Size = new System.Drawing.Size(316, 21);
            this.SelectCargo.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Selecione o Cargo";
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Enabled = false;
            this.ButtonDelete.Location = new System.Drawing.Point(148, 190);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(87, 23);
            this.ButtonDelete.TabIndex = 12;
            this.ButtonDelete.Text = "Excluir";
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // InputID
            // 
            this.InputID.Enabled = false;
            this.InputID.Location = new System.Drawing.Point(12, 25);
            this.InputID.Name = "InputID";
            this.InputID.Size = new System.Drawing.Size(58, 20);
            this.InputID.TabIndex = 15;
            this.InputID.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "ID";
            // 
            // UsuarioGerenciamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 228);
            this.Controls.Add(this.InputID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ButtonDelete);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SelectCargo);
            this.Controls.Add(this.ButtonCancelar);
            this.Controls.Add(this.ButtonCadastrar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.InputSenha);
            this.Controls.Add(this.InputUser);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "UsuarioGerenciamento";
            this.Text = "Gerenciamento de Usuário";
            this.Load += new System.EventHandler(this.TelaCadastro_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox InputUser;
        private System.Windows.Forms.TextBox InputSenha;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ButtonCadastrar;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.ComboBox SelectCargo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ButtonDelete;
        private System.Windows.Forms.TextBox InputID;
        private System.Windows.Forms.Label label6;
    }
}