﻿using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;
using DuffSnacksDesktop.Views.Categorias;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Views.Pacotes
{
    public partial class PacoteGerenciamento : Form
    {
        private int ID = -1;
        private ICollection<Product> Products = new List<Product>();
        private readonly PacksService _packsService = new PacksService();
        private readonly ProductsService _productsService = new ProductsService();
        private ICollection<Product> ProdutosSelecionados = new List<Product>();
        public PacoteGerenciamento()
        {
            InitializeComponent();
        }
        public PacoteGerenciamento(int id)
        {
            InitializeComponent();
            ID = id;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ListaProdutos_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButtonRemoverProduct.Enabled = true;

            
        }

        private void ButtonRemover_Click(object sender, EventArgs e)
        {
            // Removendo o produto da lista de produtos selecionados
            ListaProdutos.Items.Remove(ListaProdutos.SelectedItem);
            ButtonRemoverProduct.Enabled = false;
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (SelectProduct.SelectedIndex != -1)
            {
                // Adicionando o produto na lista de produtos selecionados
                ListaProdutos.Items.Add(SelectProduct.SelectedItem);
            }
        }

        private void PacoteGerenciamento_Load(object sender, EventArgs e)
        {
            // Pegando todos os produtos que sao selecionaveis
            SelectProduct.Items.Clear();
            Products = _productsService.GetAll();
            foreach (var product in Products) 
            {
                var price = product.Price.ToString("N2");
                SelectProduct.Items.Add($"{product.ID} - {product.Name} R${price}");            
            }
            // Checando se o ID e valido
            if (ID == -1) return;
            // Pegando um pacote para atualizar
            var pack = _packsService.GetOne(ID);
            // Checando se o pacote existe
            if (pack == null) return;

            // Habilitando os Botões necessarios, passando valores aos inputs
            ButtonDeletar.Enabled = true;
            InputID.Text = pack.ID.ToString();
            InputName.Text = pack.Name;
            InputDescription.Text = pack.Description;
            Disable.Checked = pack.Disabled;
            InputPrice.Text = pack.Price.ToString();
            ListaProdutos.Items.Clear();
            foreach (var packAssoc in pack.PacksAssocs)
            {
                var price = packAssoc.Product.Price.ToString("N2");
                ListaProdutos.Items.Add($"{packAssoc.ProductID} - {packAssoc.Product.Name} R${price}");
            }
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            // Convertendo o preço para double
            double.TryParse(InputPrice.Text, out var price);

            // Pegando so os IDs dos produtos selecionados
            var products = new List<int>();
            foreach (var product in ListaProdutos.Items)
                products.Add(int.Parse(product.ToString().Split("-".ToCharArray())[0].Trim()));

            if (ID == -1)
            {
                // Criando o Pacote se o ID for invalido
                if (_packsService.CreateOne(new Pack
                {
                    Name = InputName.Text,
                    Description = InputDescription.Text,
                    Price = price,
                    Disabled = Disable.Checked,
                    PacksAssocs = products.Select(x => new PackAssoc { ProductID = x }).ToList()
                })) Close();
            }
            else
            {
                // Atualizando o Pacote se o ID for valido
                if (_packsService.UpdateOne(ID, new Pack
                {
                    Name = InputName.Text,
                    Description = InputDescription.Text,
                    Price = price,
                    Disabled = Disable.Checked,
                    PacksAssocs = products.Select(x => new PackAssoc { ProductID = x, PackID = ID }).ToList()
                })) Close();
            }
        }

        private void ButtonDeletar_Click(object sender, EventArgs e)
        {
            // Deletando o pacote e Fechando a janela
            if (_packsService.DeleteOne(ID)) Close();
        }
    }
}
