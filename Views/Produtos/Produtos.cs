﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Repositories;
using DuffSnacksDesktop.Services;
using DuffSnacksDesktop.Views.Categorias;

namespace DuffSnacksDesktop.Views.Produtos
{
    public partial class Produtos : Form
    {
        private readonly ProductsService _productsService;
        private ICollection<Product> Products = new List<Product>();

        public Produtos()
        {
            InitializeComponent();
            _productsService = new ProductsService();
        }

        private void CategoryList_SelectedIndexChanged(object sender, EventArgs e)
        {
                
        }

        private void Produtos_Load(object sender, EventArgs e)
        {
            Pesquisa();
        }


        private void Pesquisa()
        {
            // Pesquisando os produtos ou retornando todos se o input for vazio
            ProductList.Items.Clear();
            Products = _productsService.GetAll(Pesquisar.Text.Trim().Split("\n".ToCharArray()).Last());
            foreach (var product in Products)
            {
                var item = new ListViewItem { Text = product.Name };
                item.SubItems.Add(product.Description);
                item.SubItems.Add(product.Price.ToString());
                var discountSimbol = product.DiscountIsPercentage ? "{0}%" : "R${0}";
                item.SubItems.Add(string.Format(discountSimbol, product.Discount.ToString()));
                var valorComDesconto = product.DiscountIsPercentage ? product.Price - product.Price / product.Discount : product.Price - product.Discount;
                item.SubItems.Add("R$" + valorComDesconto.ToString("N2"));
                item.SubItems.Add(product.Category.Name);
                ProductList.Items.Add(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }


        private void CategoryList_DoubleClick(object sender, EventArgs e)
        {
            // Quando der click duplo ele vai abrir a tela de Gerenciamento de produto
            if (ProductList.SelectedItems.Count == 0) return;
            var id = Products.ElementAt(ProductList.SelectedItems[0].Index).ID;
            var form = new ProdutoGerenciamento(id);
            form.ShowDialog();
            Pesquisa();
        }
    }
}
