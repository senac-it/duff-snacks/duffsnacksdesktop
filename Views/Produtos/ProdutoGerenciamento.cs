﻿using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Views.Produtos
{
    public partial class ProdutoGerenciamento : Form
    {
        private readonly CategoriesService _categoriesService = new CategoriesService();
        private readonly ProductsService _productsService = new ProductsService();
        private ICollection<Category> Categories = new List<Category>();
        private int ID = -1;
        public ProdutoGerenciamento()
        {
            InitializeComponent();
        }
        public ProdutoGerenciamento(int id)
        {
            InitializeComponent();
            ID = id;
        }

        private void ProdutoGerenciamento_Load(object sender, EventArgs e)
        {
            // Pegando todas as categorias e passando pro seletor de categorias
            Categories = _categoriesService.GetAll();
            SelectCategory.Items .Clear();
            foreach (var category in Categories)
                SelectCategory.Items .Add(category.Name);

            // Checando se o ID nao e invalido
            if (ID == -1) return;

            // Pegando o produto para atualizar
            var product = _productsService.GetOne(ID);
            // Checando se o produto existe
            if (product == null) return;

            // Habilitando os botões necessarios e aplicando os valores aos inputs
            ButtonDelete.Enabled = true;
            InputID.Text = product.ID.ToString();
            InputNome.Text = product.Name;
            InputPrice.Text = product.Price.ToString();
            SelectCategory.SelectedIndex = Categories
                .Select((c, i) => new { Index = i, Category = c })
                .FirstOrDefault(c => c.Category.ID == product.CategoryID)
                .Index;
            for (int i = 0; i < Categories.Count; i++)
            {
                var category = Categories.ElementAt(i);
                if (category.ID == product.CategoryID)
                {
                    SelectCategory.SelectedIndex = i;
                    break;
                }
            }
            InputDescription.Text = product.Description;
            InputDiscount.Text = product.Discount.ToString();
            InputPorcentagem.Checked = product.DiscountIsPercentage;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            double.TryParse(InputPrice.Text, out var price);
            double.TryParse(InputDiscount.Text, out var discount);
            if (ID == -1)
            {
                // Criando o produto se o ID for invalido
                if (_productsService.CreateOne(new Product
                {
                    Name = InputNome.Text,
                    Price = price,
                    CategoryID = Categories.ElementAtOrDefault(SelectCategory.SelectedIndex).ID,
                    Description= InputDescription.Text,
                    Discount= discount,
                    DiscountIsPercentage= InputPorcentagem.Checked
                })) Close();
            }
            else
            {
                // atualizando o produto se o ID for valido
                if (_productsService.UpdateOne(ID, new Product
                {
                    Name = InputNome.Text,
                    Price = price,
                    CategoryID = Categories.ElementAtOrDefault(SelectCategory.SelectedIndex).ID,
                    Description = InputDescription.Text,
                    Discount = discount,
                    DiscountIsPercentage = InputPorcentagem.Checked
                })) Close();
            }
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            // Deletando o produto e fechando a janela
            if (_productsService.DeleteOne(ID)) Close();
        }
    }
}
