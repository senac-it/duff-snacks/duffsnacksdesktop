﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models
{
    internal class Product : BaseModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public double Discount { get; set; }

        public bool DiscountIsPercentage { get; set; }

        public int CategoryID { get; set; }

        public Category Category { get; set; }

    }
}
