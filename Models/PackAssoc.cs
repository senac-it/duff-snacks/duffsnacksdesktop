﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models
{
    internal class PackAssoc : BaseModel
    {
        public int PackID { get; set; }
        public Pack Pack { get; set; }

        public int ProductID { get; set; }
        public Product Product { get; set; }

        public int Amount { get; set; }

    }
}
