﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models
{
    internal class Client : BaseModel
    {
        public string Fullname { get; set; }

        public string Email { get; set; }

        public string Document { get; set; }

        public string Address { get; set; }

    }
}
