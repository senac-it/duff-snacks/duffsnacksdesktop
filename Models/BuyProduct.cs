﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models
{
    internal class BuyProduct : BaseModel
    {
        public int ProductID { get; set; }
        public Product Product { get; set; }

        public int SaleID { get; set; }
        public Sale Sale { get; set; }

    }
}
