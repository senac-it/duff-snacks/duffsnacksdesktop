﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DuffSnacksDesktop.Configs;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Models.DTOs;
using DuffSnacksDesktop.Repositories;
using Newtonsoft.Json;
using RestSharp;

namespace DuffSnacksDesktop.Services
{
    internal class ProductsService
    {
        public ICollection<Product> GetAll(string keyword = "")
        {
            var client = Api.Client("/Products");
            var request = new RestRequest();
            request.AddParameter("keyword", keyword);
            var response = client.Get<ICollection<Product>>(request);
            return response;
        }

        public Product GetOne(int id)
        {
            var client = Api.Client($"/Products/{id}");
            var request = new RestRequest();
            var response = client.ExecuteGet<Product>(request);

            if (!response.IsSuccessStatusCode)
            {

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());

                return null;
            }

            return response.Data;
        }

        public bool CreateOne(Product data)
        {
            try
            {
                var client = Api.Client("/Products");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Name = data.Name,
                    Description = data.Description,
                    Price = data.Price,
                    Discount= data.Discount,
                    DiscountIsPercentage= data.DiscountIsPercentage,
                    CategoryID = data.CategoryID,
                });
                var response = client.ExecutePost<Product>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Produto criado com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel criar o Produto tente novamente");
                return false;
            }
        }

        public bool UpdateOne(int id, Product data)
        {
            try
            {
                var client = Api.Client($"/Products/{id}");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Name = data.Name,
                    Description = data.Description,
                    Price = data.Price,
                    Discount = data.Discount,
                    DiscountIsPercentage = data.DiscountIsPercentage,
                    CategoryID = data.CategoryID,
                });
                var response = client.ExecutePut<Product>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Produto atualizado com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar o Produto tente novamente");
                return false;
            }
        }

        public bool DeleteOne(int id)
        {
            try
            {
                var client = Api.Client($"/Products/{id}");
                var request = new RestRequest();
                var response = client.Delete<Product>(request);
                MessageBox.Show("Produto deletado");
                return true;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar o Produto tente novamente");
                return false;
            }
        }

    }
}
