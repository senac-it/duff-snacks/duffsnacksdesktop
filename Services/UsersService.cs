﻿using DuffSnacksDesktop.Configs;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Models.DTOs;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Services
{
    internal class UsersService
    {
        public ICollection<User> GetAll(string keyword = "")
        {
            try
            {
                var client = Api.Client($"/Users");
                var request = new RestRequest();
                request.AddParameter("keyword", keyword);
                var response = client.Get<ICollection<User>>(request);
                return response;
            }
            catch
            {
                MessageBox.Show("Esta ação não é permitida para este usuário");
                return null;
            }
        }
        public ICollection<Role> GetAllRoles(string keyword = "")
        {
            var client = Api.Client($"/Roles");
            var request = new RestRequest();
            request.AddParameter("keyword", keyword);
            var response = client.Get<ICollection<Role>>(request);
            return response;
        }

        public User GetOne(int id)
        {
            var client = Api.Client($"/Users/{id}");
            var request = new RestRequest();
            var response = client.ExecuteGet<User>(request);

            if (!response.IsSuccessStatusCode)
            {

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());

                return null;
            }

            return response.Data;
        }

        public bool CreateOne(User data)
        {
            try
            {
                var client = Api.Client("/Users");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Username = data.Username,
                    Password = data.Password,
                    RoleID = data.RoleID,
                }) ;
                var response = client.ExecutePost<User>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Usuário criado com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show("Esta ação não é permitida para este usuário");
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel criar o Usuário tente novamente");
                return false;
            }
        }

        public bool UpdateOne(int id, User data)
        {
            try
            {
                var client = Api.Client($"/Users/{id}");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Username = data.Username,
                    Password = data.Password,
                    RoleID = data.RoleID,
                });
                var response = client.ExecutePut<User>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Usuário atualizado com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar o Usuário tente novamente");
                return false;
            }
        }

        public bool DeleteOne(int id)
        {
            try
            {
                var client = Api.Client($"/Users/{id}");
                var request = new RestRequest();
                var response = client.Delete<User>(request);
                MessageBox.Show("Usuário deletado");
                return true;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar o Usuário tente novamente");
                return false;
            }
        }
    }
}
